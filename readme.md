# Prerequisite
Tested on Ubuntu 22.04
Modify `hosts` file as you need

## Ansible setup
`ansible-playbook playbook.yml`

# Walkthrough
## Foothold
1. Scan machine
	- ports 22 and 80 are open
1. Website redirects to `http://therage.lan`
	- add `therage.lan` to hosts file
1. Website is WordPress. Scan with any tool or browse the source code to find vulnerable plugin `pie-register v.3.7.1.4`
	- Some hints :
		- Text on front page mentions `git` (future use)
		- Text on front page mentions `custom theme`
		- Source code to identity plugin version (it's the only plugin)
1. Using exploit, get access to WordPress admin and obtain a reverse shell

## User
1. Inside WordPress directory, identity the theme folder and download the `.git` directory
1. Inside the git log, an `ssh key` has been added by mistake
1. Also, the user `walterprice` is shown as the person who did the commit
1. Using the `ssh key`, login with user `walterprice` to gain a shell

## Root
1. `walterprice` can sudo a script with a password. The command uses `rsync -aL`.
	- `rsync -L` will copy files referenced with symbolic links
1. Inside the theme folder, create a sym link to `root`
	- `ln -s /root ./root`
1. Run the command with sudo
	- `sudo /opt/update_theme.sh`
1. Navigate to `/opt/walter_price_theme/root` to gain the flag and the `.ssh` folder
1. You may now use the key to ssh in as root